import csv
import os
from typing import List
import numpy as np
import pandas as pd
from joblib import load
from getRatingsActeur import creationModele


def levenshtein_distance(s1, s2):
    if len(s1) < len(s2):
        return levenshtein_distance(s2, s1)

    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]


def find_closest_actor_name(input_name, actor_names):
    if input_name in actor_names:
        return input_name

    closest_name = None
    min_distance = float('inf')
    for i in range(len(actor_names)):
        actor_name = actor_names[i]
        distance = levenshtein_distance(input_name, actor_name)
        if distance < min_distance:
            min_distance = distance
            closest_name = actor_name
    return (closest_name)


def getUniqueActorNames(filePath):
    # Lire le fichier TSV
    df = pd.read_csv(filePath, sep='\t')
    actor_names = df['primaryName'].tolist()
    return (actor_names)


def saveUniqueActorsSorted(inputFilePath, outputFilePath):
    # Lire le fichier TSV
    df = pd.read_csv(inputFilePath, sep='\t')

    # Trier le DataFrame par 'primaryName' en ordre alphabétique
    df_sorted = df.sort_values(by='primaryName')

    # Écrire les données triées dans le fichier CSV
    with open(outputFilePath, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file, delimiter='\t')

        # Écrire l'en-tête
        writer.writerow(['primaryName', 'nconst'])

        # Écrire chaque ligne du DataFrame trié dans le fichier CSV
        for index, row in df_sorted.iterrows():
            writer.writerow([row['primaryName'], row['nconst']])


def ask_user_verification(actor_name):
    response = input(f"Est-ce que vous vouliez dire {actor_name}? (Oui/Non) ")
    return response.lower() in ['oui', 'o', 'yes', 'y']


def add_actor(actor_names):
    user_input = input("Entrez le nom de l'acteur à ajouter : ")
    closest_name = find_closest_actor_name(user_input, actor_names)
    if ask_user_verification(closest_name):
        return closest_name
    else:
        user_choice = input("Voulez-vous réessayer avec un autre nom? (Oui/Non) ")
        if user_choice.lower() in ['oui', 'o', 'yes', 'y']:
            return add_actor(actor_names)
        else:
            return None


def calculPrevision(listNomsActeurs, dfActeurs, clf):
    if len(listNomsActeurs) == 4:
        print('\nPrédiction en cours...\n')
        notesActeurs = []
        for nom in listNomsActeurs:
            note = dfActeurs.loc[dfActeurs.primaryName == nom].averageRatingMean.values[0]
            print(nom, " a pour note moyenne : ", note)
            notesActeurs.append(note)
        prediction = clf.predict([notesActeurs])[0]
        print("\nNote prédite : ", "{:.2f}".format(prediction), "\n")
    else:
        print("La liste d'acteurs n'est pas de la bonne taille")


def find_direct_actor_name(df, searchedValue):
    if 'primaryName' in df.columns:
        if searchedValue in df['primaryName'].values:
            return searchedValue
    return None


if __name__ == "__main__":
    mustContinue = True
    if not os.path.exists("processedData/actorsRatingsGroupedWithName.tsv"):
        print("Veuillez lancer getRatingsActeur.py avant d'exécuter ce programme.")
        exit()
    dfActeurs = pd.read_csv("processedData/actorsRatingsGroupedWithName.tsv", sep="\t")
    if not os.path.exists("processedData/uniqueActorNames.tsv"):
        saveUniqueActorsSorted("processedData/actorsRatingsGroupedWithName.tsv", "processedData/uniqueActorNames.tsv")
    if not os.path.exists("processedData/modele.joblib"):
        creationModele()
    clf = load('processedData/modele.joblib')
    actor_names = getUniqueActorNames("processedData/uniqueActorNames.tsv")
    selectedActorNames = []
    print("Bienvenue dans MoviePrecog!")
    while mustContinue:
        print("1: Ajouter un acteur à la liste (4 acteurs / actrices requis)")
        print("2: Afficher la liste")
        print("3: Vider la liste")
        print("4: Lancer la prévision")
        print("5: Quitter")

        choice = input("Faites votre choix : ")

        if choice == '1':
            result = add_actor(actor_names)
            if result:
                selectedActorNames.append(result)
                print(f"{result} a été ajouté à la liste.")
        elif choice == '2':
            print("Liste des acteurs sélectionnés :")
            for actor in selectedActorNames:
                print(actor)
        elif choice == '3':
            selectedActorNames.clear()
            print("La liste a été vidée.")
        elif choice == '4':
            calculPrevision(selectedActorNames, dfActeurs, clf)
        elif choice == '5':
            print("Au revoir !")
            mustContinue = False
        else:
            print("Choix non valide, veuillez réessayer.")
