# MoviePrecog

## Description du projet

MoviePrecog est un projet de BUT3 réalisé par Paul Squizzato et Aurélien Pintrand. Le but est de créer un modèle de prévision à partir d'une base de données de films et d'acteurs (prise depuis le site imdb). Ce modèle permet d'obtenir une estimation de la note moyenne d'un film imaginaire fourni (avec des acteurs réels).

## La base de données 

Pour lancer le programme, vous aurez besoin, dans un premier temps, de télécharger [les bases de données requises](https://developer.imdb.com/non-commercial-datasets/).
Vous aurez besoin de télécharger les bases suivantes:
- name.basics.tsv : contient une liste d'acteurs, ainsi que les films principaux pour lesquels ils sont connus
- title.principals.tsv : contient une lsite de films, ainsi que les principales personnes qui ont travailées dessus et leur rôle (compositeur, acteur/actrice, réalisateur... etc)
- title.ratings.tsv : contient la liste de films, la note moyenne associée et le nombre d'utilisateurs qui ont donné une note

## Lancement du programme

- Si jamais, pour une raison ou pour une autre, le dossier processedData ne contient pas tous les fichiers, vous pouvez les initialiser en exécutant le script python getRatingsActeur.py
- Une fois tous les fichiers initialisés, il vous suffit de lancer le script interface.py et de vous laisser guider par l'interface